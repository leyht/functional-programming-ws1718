{-# LANGUAGE TemplateHaskell #-}

module Sheet2 where

import Test.QuickCheck
import Data.List

-- Exercise 1
--------------------------------

-- Naive Fibonacci
fib :: Integer -> Integer
fib 0 = 0
fib 1 = 1
fib n
    | n < 0 = undefined
    | otherwise = fib (n-1) + fib (n-2)

-- A better Fibonacci function using dynamic programming
fibDyn :: Integer -> Integer
fibDyn n = head (fibList n)

-- This is a helper function for generating a list of Fibonacci numbers
fibList :: Integer -> [Integer]
fibList 0 = [0]
fibList 1 = [1,0]
fibList n
    | n < 0 = undefined
    | otherwise = (sum $ take 2 lastList):lastList
        where lastList = fibList (n-1)

-- Limit the test size? Seems I missed the part in the lecture where this was done...
prop_Fib n
    | (0 <= n) && (n < 30) = fibDyn n == fib n
    | otherwise = True

-- Exercise 2
--------------------------------

undup :: (Eq a) => [a] -> [a]
undup [] = []
undup (x:xs) = x:undup (removeElement x xs)

prop_undupWorks x = undup x == nub x

-- Removes all occurences of an element from a list.
removeElement :: (Eq a) => a -> [a] -> [a]
removeElement _ [] = []
removeElement y (x:xs) = if x == y
                         then removeElement y xs
                         else x:removeElement y xs

-- Exercise 3
--------------------------------

smallestFactor1 :: Integer -> Integer
smallestFactor1 n
    | n >= 2    = fst $ head factors
    | otherwise = n
    where factors = [(k,m) | k <- [2..n], m <- [1..n], k*m == n]

smallestFactor2 :: Integer -> Integer
smallestFactor2 n
    | n >= 2    = head [k | k <- [2..], n `mod` k == 0]
    | otherwise = n

prop_factors n = smallestFactor1 n == smallestFactor2 n

-- Testing all properties 
return []
runTests = $quickCheckAll

-- Exercise 4
--------------------------------

type Title = String
type Artist = String
type Duration = Integer
type Track = (Title, Artist, Duration)
type Album = [Track]
data Rating = Good | Bad
    deriving (Show)
type User = String
type MediaBib = [(Album,[User])]

addAlbum :: Track -> Album -> MediaBib -> MediaBib
addAlbum _ _ [] = []
addAlbum track album' ((album,users):bib) =
    if album' == album
        then ((track : album), users) : bib
        else entry : (addAlbum track album' bib)
    where entry = (album,users)

-- I don't have an idea for the ratings.
-- And the MediaBib is really ugly...

returnAlbums :: MediaBib -> [(Album, Duration)]
returnAlbums [] = []
returnAlbums ((album,_):bib) = (album, duration) : returnAlbums bib
        where duration = sum (third (unzip3 album))

third :: (a,b,c) -> c
third (_,_,x) = x

-- Exercise 5
--------------------------------

data Tick = X | O | E  -- E for empty
        deriving (Show, Eq)
type Player = Tick
type Col = (Tick, Tick, Tick)
type Field = (Col, Col, Col)
data State = Xwon | Owon | InProgress | Invalid
        deriving (Show, Eq)

emptyField = ((E,E,E),(E,E,E),(E,E,E))
invalidField = ((O,O,O),(E,E,E),(X,X,X))
winnerField = ((X,E,O),
               (E,X,O),
               (E,E,X))
cheaterField = ((X,E,O),
                (E,X,E),
                (E,E,X))

currentState :: Field -> State
currentState field
    | didXwin && didOwin = Invalid
    | differenceOfMoves > 1 = Invalid
    | didOwin = Owon
    | didXwin = Xwon
    | otherwise = InProgress
    where didXwin = checkWinner X field
          didOwin = checkWinner O field
          differenceOfMoves = abs ((countMoves X field) - (countMoves O field))

whoWon :: Field -> Player
whoWon field
    | currentState field == Owon = O
    | currentState field == Xwon = X
    | otherwise                  = E

checkWinner :: Player -> Field -> Bool
checkWinner player ((a1,a2,a3),(b1,b2,b3),(c1,c2,c3))
    | identic a1 a2 a3 && a1 == player = True
    | identic b1 b2 b3 && b1 == player = True
    | identic c1 c2 c3 && c1 == player = True
    | identic a1 b1 c1 && a1 == player = True
    | identic a2 b2 c2 && a2 == player = True
    | identic a3 b3 c3 && a3 == player = True
    | identic a1 b2 c3 && a1 == player = True
    | identic a3 b2 c1 && a3 == player = True
    | otherwise = False

identic :: (Eq a) => a -> a -> a -> Bool
identic x y z
        | (x == y) && (x == z) = True
        | otherwise            = False

fieldToList :: Field -> [Tick]
fieldToList ((a1,a2,a3),(b1,b2,b3),(c1,c2,c3)) = [a1,a2,a3,b1,b2,b3,c1,c2,c3]

countMoves :: Player -> Field -> Int
countMoves player field = sum $ map (\x -> if x == player then 1 else 0) (fieldToList field)