{-# LANGUAGE TemplateHaskell #-}

module Sheet1 where

import Test.QuickCheck
import Data.List

-- Exercise 1
--------------------------------

maxi :: Int -> Int -> Int
maxi x y
    | x > y     = x
    | otherwise = y

mini :: Int -> Int -> Int
mini x y
    | x < y     = x
    | otherwise = y

max3 :: Int -> Int -> Int -> Int
max3 x y z
    | x >= y && x >= z = x
    | y >= x && y >= z = y
    | otherwise        = z

med :: Int -> Int -> Int -> Int
med x y z
    | x <= z && x >= y = x
    | x >= z && x <= y = x
    | y <= z && y >= x = y
    | y >= z && y <= x = y
    | otherwise        = z

prop_MaxIsMax x y = max x y == maxi x y

prop_MinIsMin x y = min x y == mini x y

prop_Max3IsMax x y z = last (sort [x,y,z]) == max3 x y z

prop_MedIsMed x y z = sort [x,y,z] !! 1 == med x y z

-- Exercise 2
--------------------------------

type Stack = [Int]

push :: Int -> Stack -> Stack
push x xs = x:xs

pop :: Stack -> Stack
pop [] = []
pop (x:xs) = xs

dup :: Stack -> Stack
dup [] = []
dup (x:xs) = x:x:xs

add :: Stack -> Stack
add [] = []
add [x] = [x]
add (x:y:xs) = (x + y):xs

substract :: Stack -> Stack
substract [] = []
substract [x] = [x]
substract (x:y:xs) = (x - y):xs

multiply :: Stack -> Stack
multiply [] = []
multiply [x] = [0]
multiply (x:y:xs) = (x * y):xs

neg :: Stack -> Stack
neg [] = []
neg (x:xs) = -x:xs

prop_push x y = head (push x y) == x
prop_pop [] = pop [] == []
prop_pop x = pop x == tail x
prop_dup [] = dup [] == []
prop_dup x = dup x == (head x):x
prop_add [] = add [] == []
prop_add x = head (add x) == sum (take 2 x)
prop_substract [] = substract [] == []
prop_substract [x] = substract [x] == [x]
prop_substract x = head (substract x) == ((x !! 0) - (x !! 1))
prop_multiply [] = multiply [] == []
prop_multiply [x] = multiply [x] == [0]
prop_multiply x = head (multiply x) == ((x !! 0) * (x !! 1))
prop_neg [] = neg [] == []
prop_neg x = head (neg x) == -(head x)

-- Just write the commands after another separated by whitespace.
readCommand :: String -> Stack -> Stack
readCommand [] stack    = stack
readCommand ('p':'u':'s':'h':' ':xs) stack = readCommand remaining (push n stack)
    where n = read (head (words xs)) :: Int
          remaining = unwords (tail (words xs))
readCommand input stack = readCommand remaining ((runCommand command) stack)
    where command = head (words input)
          remaining = unwords (tail (words input))

runCommand :: String -> (Stack -> Stack)
runCommand "pop" = pop
runCommand "dup" = dup
runCommand "add" = add
runCommand "substract" = substract
runCommand "multiply" = multiply
runCommand "neg" = neg
runCommand _ = id

-- Exercise 3
--------------------------------

head' :: [a] -> a
head' (x:xs) = x
-- because the operation on the empty list is not defined
prop_HeadIsHead [] = True 
prop_HeadIsHead x = head x == head' x

tail' :: [a] -> [a]
tail' (x:xs) = xs
prop_TailIsTail [] = True
prop_TailIsTail x = tail x == tail' x

init' :: [a] -> [a]
init' [x] = []
init' (x:xs) = x:init' xs
prop_InitIsInit [] = True
prop_InitIsInit x = init x == init' x

last' :: [a] -> a
last' [x] = x
last' (x:xs) = last' xs
prop_LastIsLast [] = True
prop_LastIsLast x = last x == last' x

length' :: [a] -> Int
length' []   = 0
length' (x:xs) = 1 + length' xs
prop_LenIsLen x = length x == length' x

reverse' :: [a] -> [a]
reverse' [] = []
reverse' [x] = [x]
reverse' (x:xs) = reverse' xs ++ [x]
prop_RevIsRev x = reverse x == reverse' x

(+++) :: [a] -> [a] -> [a]
(+++) [] right = right
(+++) (x:xs) right = x:(xs +++ right)
prop_AppendIsAppend x y = (++) x y == (+++) x y

iterate' :: (a -> a) -> a -> [a]
iterate' f x = (f x):(iterate' f x)
-- How to use QuickCheck on this where the input needs to be a function?

map' :: (a -> b) -> [a] -> [b]
map' f [] = []
map' f (x:xs) = (f x):(map' f xs)

filter' :: (a -> Bool) -> [a] -> [a] 
filter' f [] = []
filter' f (x:xs) = if f x
                     then x:filter' f xs
                     else   filter' f xs

intersperse' :: a -> [a] -> [a]
intersperse' _ [] = []
intersperse' _ [x] = [x]
intersperse' y (x:xs) = x:y:intersperse' y xs
prop_InterIsInter x y = intersperse x y == intersperse' x y

concat' :: [[a]] -> [a]
concat' [] = []
concat' [x] = x
concat' (x:xs) = x +++ (concat' xs)
prop_ConcatIsConcat x = concat x == concat' x

zipWith' :: (a -> b -> c) -> [a] -> [b] -> [c]
zipWith' _ _ [] = []
zipWith' _ [] _ = []
zipWith' f (x:xs) (y:ys) = (f x y):zipWith' f xs ys

repeat' :: a -> [a]
repeat' x = x:repeat' x
prop_RepeatIsRepeat x = (take 10 $ repeat x) == (take 10 $ repeat' x)

and' :: [Bool] -> Bool
and' [] = True
and' (False:xs) = False
and' (True:xs)  = and' xs
prop_AndIsAnd x = and x == and' x

takeWhile' :: (a -> Bool) -> [a] -> [a]
takeWhile' _ [] = []
takeWhile' f (x:xs) = if f x
                        then x:takeWhile' f xs
                        else []

dropWhile' :: (a -> Bool) -> [a] -> [a] 
dropWhile' _ [] = []
dropWhile' f (x:xs) = if f x
                        then dropWhile' f xs
                        else x:xs

maximum' :: Ord a => [a] -> a
maximum' [x] = x
maximum' (x:xs) = if x > x'
                    then x
                    else x'
    where x' = maximum' xs
prop_MaximumIsMaximum :: [Int] -> Bool
prop_MaximumIsMaximum [] = True
prop_MaximumIsMaximum x = maximum x == maximum' x

-- See <https://hackage.haskell.org/package/QuickCheck-2.10.1/docs/Test-QuickCheck-All.html>
return []
runTests = $quickCheckAll